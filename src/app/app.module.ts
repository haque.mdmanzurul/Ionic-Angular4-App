import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ModalContentPage } from '../pages/home/home';
import { AddToCartPage } from '../pages/home/home';
import { FlavorPopup } from '../pages/flavorPopup/flavorPopup';

import { HttpModule } from '@angular/http';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireModule } from 'angularfire2';
import { FirebaseProvider } from './../providers/firebase/firebase';

const firebaseConfig = {
  apiKey: "AIzaSyAwKYeAorMn744T-npRNAUOT7wQ9Dnpof4",
  authDomain: "icecream-app-aa54e.firebaseapp.com",
  databaseURL: "https://icecream-app-aa54e.firebaseio.com",
  projectId: "icecream-app-aa54e",
  storageBucket: "icecream-app-aa54e.appspot.com",
  messagingSenderId: "84290663114"
};


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ModalContentPage,
    AddToCartPage,
    FlavorPopup
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(firebaseConfig),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ModalContentPage,
    AddToCartPage,
    FlavorPopup
  ],
  providers: [
    StatusBar,
    SplashScreen,
    FirebaseProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    FirebaseProvider
  ]
})
export class AppModule {}
