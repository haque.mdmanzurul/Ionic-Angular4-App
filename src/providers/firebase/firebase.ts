import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import * as _ from "lodash";

@Injectable()
export class FirebaseProvider {

  constructor(public afd: AngularFireDatabase) { }


  /*addItem(name) {
    this.afd.list('/shoppingItems/').push(name);
  }

  removeItem(id) {
    this.afd.list('/shoppingItems/').remove(id);
  }*/

  getShopLists() {
    return this.afd.list('/shopList/');
  }

  getFlavours() {
    return this.afd.list('/flavors/');
  }

  getShopFlavors($key) {
    let shops, flavors = null;
    this.getShopLists().subscribe((res) => {
      shops = res;
      _.forEach(flavors, function(value, key){
        if($key == key)
          flavors = value.availableFlavors.split(',');
      });
    });

    return flavors;
  }
}
