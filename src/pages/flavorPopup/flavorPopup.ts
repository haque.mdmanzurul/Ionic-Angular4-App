import { Component } from '@angular/core';
import { NavController, ModalController, Platform, NavParams, ViewController } from 'ionic-angular';
import { FirebaseProvider } from './../../providers/firebase/firebase';
import { FirebaseListObservable } from 'angularfire2/database';
import * as _ from "lodash";

// Add To Cart Form
@Component({
  selector: 'flavor-popup',
  templateUrl: 'flavorPopup.html'
})
export class FlavorPopup {
  flavors: any;
  order: any;
  flvNo: any;
  flv: any;

  constructor(
    public platform: Platform,
    public params: NavParams,
    public viewCtrl: ViewController,
    public firebaseProvider: FirebaseProvider,

  ) {

    let shop = this.params.get('shop');
    let shopId = this.params.get('shopId');
    this.order = this.params.get('order');
    this.flvNo = this.params.get('flvNo');
    this.flv = this.params.get('flvNo');
    this.firebaseProvider.getFlavours().subscribe((res) => {
      let Flavors : any[] = res;
      let shopFlavors = shop.availableFlavors.split(",");
      let returnFlavors : any[] = [];
      _.forEach(Flavors, function(value, key){
        if(shopFlavors.indexOf(value.$key) >= 0)
          returnFlavors[key] = value;

      });
      this.flavors = returnFlavors;
    });
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
  selectFlavor(flavor) {
    this.order[this.flvNo] = flavor;
    this.flv = flavor;
    console.log(this.flv);
    this.dismiss();
  }
}
