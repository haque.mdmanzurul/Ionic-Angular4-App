import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FirebaseProvider } from './../../providers/firebase/firebase';
import { FirebaseListObservable } from 'angularfire2/database';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  shoplists: FirebaseListObservable<any[]>;
  newItem = '';

  showShopList = true;
  selectedShop = null;

  constructor(public navCtrl: NavController, public firebaseProvider: FirebaseProvider) {
    this.shoplists = this.firebaseProvider.getShopLists();
    console.log('', this.shoplists)
  }

  goToShop(id) {
    this.selectedShop = this.shoplists[id];
    this.showShopList = false;
  }

  addToCart(id) {

  }

}
