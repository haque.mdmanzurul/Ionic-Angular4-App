import { Component } from '@angular/core';
import { NavController, ModalController, Platform, NavParams, ViewController } from 'ionic-angular';
import { FirebaseProvider } from './../../providers/firebase/firebase';
import { FirebaseListObservable } from 'angularfire2/database';
import { FlavorPopup } from '../flavorPopup/flavorPopup';
import * as _ from "lodash";


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  shoplists: any[];
  newItem = '';

  showShopList = true;
  selectedShop = null;

  myOrders: any[] = [];

  constructor(public navCtrl: NavController,
              public modalCtrl: ModalController,
              public firebaseProvider: FirebaseProvider
  ) {
    this.firebaseProvider.getShopLists().subscribe((res) => {
        this.shoplists = res;
        console.log(this.shoplists);
      }
    );
  }

  goToShop(id) {
    let shop = this.shoplists[id];
    this.myOrders = [];
    let modal = this.modalCtrl.create(ModalContentPage, {shopId: id, shop: shop, orders: this.myOrders});
    modal.present();
  }

  addToCart() {

  }

}

@Component({
  template: `
<ion-header>
  <ion-toolbar>
    <ion-title>
      {{selectedShop.name}}
    </ion-title>
    <ion-buttons start>
      <button ion-button (click)="dismiss()">
        <span ion-text color="primary" showWhen="ios">Cancel</span>
        <ion-icon name="md-close" showWhen="android, windows"></ion-icon>
      </button>
    </ion-buttons>
  </ion-toolbar>
</ion-header>

<ion-content>
  <ion-row>
    <button ion-button (click)="addToCart()">Add Item to Order</button>
  </ion-row>
</ion-content>
<ion-list *ngIf="myOrders.length">
    <ion-card-header>My Orders</ion-card-header>
    <ion-item-sliding *ngFor="let order of myOrders; let i = index">
      <ion-item>
        <ion-label>{{ order.forName }}</ion-label>
      </ion-item>
    </ion-item-sliding>
  </ion-list>
`
})
export class ModalContentPage {
  selectedShop;
  description;
  shopId;
  myOrders: any = [];

  constructor(
    public platform: Platform,
    public params: NavParams,
    public viewCtrl: ViewController,
    public firebaseProvider: FirebaseProvider,
    public modalCtrl: ModalController
  )
  {
    this.shopId = this.params.get('shopId');
    this.selectedShop  = this.params.get('shop');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
  addToCart() {
    let modal = this.modalCtrl.create(AddToCartPage, {shop: this.selectedShop, shopId: this.shopId, orders: this.myOrders});
    modal.present();
  }
}

// Add To Cart Form
@Component({
  template: `
<ion-header>
  <ion-toolbar>
    <ion-title>
      Add To Cart
    </ion-title>
    <ion-buttons start>
      <button ion-button (click)="dismiss()">
        <span ion-text color="primary" showWhen="ios">Cancel</span>
        <ion-icon name="md-close" showWhen="android, windows"></ion-icon>
      </button>
    </ion-buttons>
  </ion-toolbar>
</ion-header>

<ion-content>
  <ion-row>
    <ion-col col-7>
    <ion-list>
        <ion-item>
          <ion-label>Weight(kg)</ion-label>
          <ion-select name="weight" [(ngModel)]="weight">
            <ion-option value="1">1</ion-option>
            <ion-option value="2">2</ion-option>
            <ion-option value="3">3</ion-option>
            <ion-option value="4">4</ion-option>
          </ion-select>
        </ion-item>
      </ion-list>
    </ion-col>
    
    <ion-col col-5>
    <ion-label> Price : {{shop.price}}$</ion-label>
    </ion-col>
    
  </ion-row>
  <ion-row>
    <ion-col col-12>
    <ion-item>
    <ion-label fixed>For</ion-label>
    <ion-input type="text" value="" [(ngModel)]="forName"></ion-input>
  </ion-item>

    </ion-col>
  </ion-row>
  <ion-row>
    <ion-list>
  
      <ion-item>
        <button ion-button (click)="showShopFlavors(shopId,'flavor1', flavor1)">Flavor 1</button>
        <ion-card *ngIf="flavor1 != null">
          <ion-card-content>
          <ion-label>
          <ion-img src="flavor1.image"></ion-img><br/> {{flavor1.name}}
          </ion-label>
          </ion-card-content>
        </ion-card>
      </ion-item>
      
      <ion-item>
        <button ion-button (click)="showShopFlavors(shopId, 'flavor2', flavor2)">Flavor 2</button>
        <ion-card *ngIf="flavor2 != null">
          <ion-card-content>
          <ion-label>
          <ion-img src="flavor2.image"></ion-img><br/> {{flavor2.name}}
          </ion-label>
          </ion-card-content>
        </ion-card>
      </ion-item>
      
      <ion-item>
        <button ion-button (click)="showShopFlavors(shopId, 'flavor3', flavor3)">Flavor 3</button>
        <ion-card *ngIf="flavor3 != null">
          <ion-card-content>
          <ion-label>
          <ion-img src="flavor3.image"></ion-img><br/> {{flavor3.name}}
          </ion-label>
          </ion-card-content>
        </ion-card>
      </ion-item>
  </ion-list>
   
</ion-row>
  <ion-row>
    <button ion-button (click)="addToCart()">Add Item to Order</button>
  </ion-row>
</ion-content>
`})
export class AddToCartPage {
  shop = null;
  shopId = null;
  flavours = [];
  myOrders: any;

  //model
  order: any = [];
  forName: any = null;
  weight: any = null;
  flavor1: any = null;
  flavor2: any = null;
  flavor3: any = null;
  constructor(
    public platform: Platform,
    public params: NavParams,
    public viewCtrl: ViewController,
    public firebaseProvider: FirebaseProvider,
    public modalCtrl: ModalController

  ) {

    //this.selectedShop = this.params.get('shop');
    //console.log(this.selectedShop)
    this.shop = this.params.get('shop');
    console.log('This is selected',this.shop);
    this.shopId = this.params.get('shopId');
    console.log('This is shopid',this.shopId);
    this.myOrders = this.params.get('orders');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
  addToCart() {
    this.order['forName'] = this.forName;
    this.order['weight'] = this.weight;
    this.order['flavor1'] = this.flavor1;
    this.order['flavor2'] = this.flavor2;
    this.order['flavor3'] = this.flavor3;

    this.myOrders.push(this.order);
    console.log(this.myOrders);
    this.dismiss();
  }

  showShopFlavors(shopId, flvNo, flv) {
    console.log('Passing shopid To modal', this.params.get('shopId'))
    let modal = this.modalCtrl.create(FlavorPopup, {shopId: this.shopId, shop: this.shop, flvNo: flvNo, order: this.order, flv: flv});
    modal.present();
  }

}
